const User = require('../models/User');

exports.getUser = async ctx => {
  // todo: enable more complex filtering, paging etc
  const query = ctx.query || {};
  ctx.body = await User.find(query);
};

exports.getOne = async ctx => {
  const id = ctx.params.id;
  const query = ctx.query;

  const user = query.byToken ? await User.findOne({ username: id }) : await User.findById(id);

  if (!user) {
    ctx.throw(404, 'User not found');
  }
  ctx.body = user;
};

exports.createUser = async ctx => {
  let values = ctx.request.body;
  let newUser = await User.create(values);

  if (!newUser || !newUser._id) {
    ctx.throw(500, 'Error creating user');
  }
  ctx.body = newUser;
};

exports.updateUser = async ctx => {
  const id = ctx.params.id;
  const values = ctx.request.body;

  let foundUser = await User.findById(id);

  if (!foundUser || !foundUser._id) {
    ctx.throw(404, 'User not found');
  }

  let updated = await User.findByIdAndUpdate(id, values, { new: true });

  if (!updated || !updated._id) {
    ctx.throw(500, 'Error updating item');
  }

  ctx.body = updated;
};

exports.deleteUser = async ctx => {
  const id = ctx.params.id;

  const user = await User.findById(id);
  if (!user) {
    ctx.throw(404, 'User Class not found');
  }

  let deletedUser = await User.findByIdAndRemove(id);

  if (!deletedUser) {
    ctx.throw(500, 'Error deleting user');
  }

  ctx.body = deletedUser;
};
